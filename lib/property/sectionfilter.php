<?php

namespace Project\Seo\Props\Property;

use Bitrix\Main\Application;
use CIBlock;
use CIBlockElement;
use CIBlockProperty;
use CIBlockSectionPropertyLink;
use CUserTypeInteger;
use Project\Seo\Props\Config;
use Project\Seo\Props\Database;


class SectionFilter extends CUserTypeInteger
{
    protected $isRequestViaAjax;

    // инициализация пользовательского свойства для главного модуля
    function GetUserTypeDescription()
    {
        return [
            'PROPERTY_TYPE'        => 'S',
            'USER_TYPE'            => __CLASS__,
            'DESCRIPTION'          => "Привязка к свойствам смартфильтра инфоблока",
            'GetPropertyFieldHtml' => [__CLASS__, 'GetPropertyFieldHtml'],
            'GetAdminListViewHTML' => [__CLASS__, 'GetAdminListViewHTML'],
        ];
    }

    // инициализация пользовательского свойства для инфоблока
    function GetIBlockPropertyDescription()
    {
        return [
            "PROPERTY_TYPE"        => "S",
            "USER_TYPE"            => __CLASS__,
            "DESCRIPTION"          => "Привязка к свойствам смартфильтра инфоблока",
            'GetPropertyFieldHtml' => [__CLASS__, 'GetPropertyFieldHtml'],
            'GetAdminListViewHTML' => [__CLASS__, 'GetAdminListViewHTML'],
        ];
    }

    // представление свойства
    function getViewHTML($name, $value)
    {
        $IBLOCK_ID2 = 23;
        $selectedValues = explode("::", $value);

        if (is_array($selectedValues) && count($selectedValues) > 1) {
            $selectedItem = $selectedValues[0];
            $selectedValue = $selectedValues[1];
        }
        $res = CIBlock::GetByID($IBLOCK_ID2);
        $proplist = CIBlockProperty::GetList(["NAME" => "ASC", "SORT" => "ASC"], [
            "CODE"          => $selectedValues[0],
            "FILTRABLE"     => "Y",
            "PROPERTY_TYPE" => "S",
            "!USER_TYPE"    => "",
            "ACTIVE"        => "Y",
            "IBLOCK_ID"     => $IBLOCK_ID2,
        ]);

        while ($ar_property = $proplist->GetNext()) {
            $value = $ar_property["NAME"] . ':' . $selectedValue;

        }
        return $value;

    }


    // редактирование свойства
    function getEditHTML($name, $value, $is_ajax = false)
    {
        $return = '<input type="hidden" name="' . $name . '" value="' . $value . '" class="SectionFilterPropListValue">';

        $IBLOCK_ID2 = Config::CATALOG_ID;
        $selectedValues = explode("::", $value);

        if (is_array($selectedValues) && count($selectedValues) > 1) {
            $selectedItem = $selectedValues[0];
            $selectedValue = $selectedValues[1];
        } else {
            $selectedItem = $selectedValues[0];
            $selectedValue = false;
        }
        $arIBlock = CIBlock::GetByID($IBLOCK_ID2)->Fetch();


        $res = CIBlockElement::GetList(
            [],
            [
                'IBLOCK_ID' => Config::SEO_ID,
                'ID'        => (int)Application::getInstance()->getContext()->getRequest()->get('ID'),
            ],
            false,
            false,
            ['PROPERTY_SECTION_ID']
        );
        if ($arItem = $res->Fetch()) {
//            $res = Database\SectionTable::getList([
//                'select' => ['PROPS_ID'],
//                'filter' => [
//                    'SECTION_ID' => $arItem['PROPERTY_SECTION_ID_VALUE'],
//                ],
//            ]);
//            $arPropsSection = [];
//            while ($arItem = $res->fetch()) {
//                $arPropsSection[$arItem['PROPS_ID']] = $arItem['PROPS_ID'];
//            }

//            pre($arPropsSection);


            $arPropLinks = CIBlockSectionPropertyLink::GetArray($arIBlock["ID"], $arItem['PROPERTY_SECTION_ID_VALUE'], $arItem['PROPERTY_SECTION_ID_VALUE'] <= 0);

            $proplist = CIBlockProperty::GetList(["NAME" => "ASC", "SORT" => "ASC"], [
                "FILTRABLE"     => "Y",
                "PROPERTY_TYPE" => "S",
                "!USER_TYPE"    => "",
                "ACTIVE"        => "Y",
                "IBLOCK_ID"     => $IBLOCK_ID2,
            ]);

            $blockItems = '<select class="SectionFilterPropList-items" width="30">';
            $blockItems .= '<option value="" ' . (!$selectedItem ? 'selected' : '') . '>(не установлено)</option>';

            $blockValues .= '<select class="SectionFilterPropList-values ' . ($selectedItem ? 'active' : 'off') . '">';
            $blockValues .= '<option value="" ' . (!$selectedValue ? 'selected' : '') . '>Выберите значение</option>';

            while ($ar_property = $proplist->GetNext()) {
                if ($ar_property['CODE'] == 'PROIZVODITEL' || $ar_property['CODE'] == 'COLOR') {
                    continue;
                }

                //print_r($ar_property);
                //$return .= '<h3>'.$ar_property['NAME'].'('.$ar_property['CODE'].')</h3>';
                $blockItems .= '<option value="' . $ar_property["ID"] . '" ' . ($ar_property["CODE"] === $selectedItem ? 'selected' : '') . ' data-code="' . $ar_property['CODE'] . '" >' . $ar_property['NAME'] . ' (' . $ar_property['CODE'] . ')</option>';

                $blockValues .= '<optgroup label="' . $ar_property['NAME'] . ' (' . $ar_property['CODE'] . ')" class="SectionFilterPropList-blockItems ' . ($ar_property["CODE"] === $selectedItem ? 'active' : 'off') . ' SectionFilterPropList-blockItems-' . $ar_property["ID"] . '">';
                $arItemValues = [];

                $rsResult = CIBlockElement::GetList(
                    [],
                    ['!PROPERTY_' . $ar_property["ID"] . '_VALUE' => '', 'ACTIVE' => 'Y', 'IBLOCK_ID' => $IBLOCK_ID2],
                    ['PROPERTY_' . $ar_property["ID"]], false, []);


                while ($arResult = $rsResult->Fetch()) {


                    if ($arResult['PROPERTY_' . $ar_property["ID"] . '_VALUE']) {
                        $arItemValues[$arResult['PROPERTY_' . $ar_property["ID"] . '_VALUE']] = [
                            "VALUE"         => $arResult['PROPERTY_' . $ar_property["ID"] . '_VALUE'],
                            "CNT"           => $arResult['CNT'],
                            "CHECKED"       => $arResult['PROPERTY_' . $ar_property["ID"] . '_VALUE'] === $selectedValue && $ar_property["CODE"] === $selectedItem ? true : false,
                            "VALUE_ENCODED" => urlencode(urlencode(strtolower($arResult['PROPERTY_' . $ar_property["ID"] . '_VALUE']))),
                        ];
                    }
                }
                uksort($arItemValues, 'strnatcasecmp');
                foreach ($arItemValues as $arItemValue) {
                    $blockValues .= '<option value="' . $arItemValue['VALUE'] . '" ' . ($arItemValue['CHECKED'] ? 'selected' : '') . ' data-encValue="' . $arItemValue['VALUE_ENCODED'] . '">' . $arItemValue['VALUE'] . ' (' . $arItemValue['CNT'] . ')</option>';
                }

                $blockValues .= '</optgroup>';
            }
        }


        $blockValues .= '</select>';
        $blockItems .= '</select>';

        $return .= $blockItems;
        $return .= $blockValues;
        $return .= '<br/><br/>';


        return '
  
  ' . $return;

    }


    // редактирование свойства в форме (главный модуль)
    function GetEditFormHTML($arUserField, $arHtmlControl)
    {
        return self::getEditHTML($arHtmlControl['NAME'], $arHtmlControl['VALUE'], false);
    }

    // редактирование свойства в списке (главный модуль)
    function GetAdminListEditHTML($arUserField, $arHtmlControl)
    {
        return self::getViewHTML($arHtmlControl['NAME'], $arHtmlControl['VALUE'], true);
    }

    // представление свойства в списке (главный модуль, инфоблок)
    function GetAdminListViewHTML($arProperty, $value, $strHTMLControlName)
    {
        return self::getViewHTML($strHTMLControlName['VALUE'], $value['VALUE']);
    }

    // редактирование свойства в форме и списке (инфоблок)
    function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
    {
        return $strHTMLControlName['MODE'] == 'FORM_FILL'
            ? self::getEditHTML($strHTMLControlName['VALUE'], $value['VALUE'], false)
            : '-'//self::getViewHTML($strHTMLControlName['VALUE'], $value['VALUE'])
            ;
    }

}