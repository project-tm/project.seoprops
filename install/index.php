<?php

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Project\Tools\Modules;

Loc::loadMessages(__FILE__);

class project_seoprops extends CModule
{
    public $MODULE_ID = 'project.seoprops';
    public $MODULE_NAME;
    public $MODULE_DESCRIPTION;
    public $MODULE_VERSION;
    public $MODULE_VERSION_DATE;

    use Modules\Install;

    function __construct()
    {
        $this->setParam(__DIR__, '');
        $this->MODULE_NAME = Loc::getMessage('PROJECT_SEO_PROPS_MODULE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('PROJECT_SEO_PROPS_DESCRIPTION');
        $this->PARTNER_NAME = Loc::getMessage('PROJECT_SEO_PROPS_PARTNER_NAME');
        $this->PARTNER_URI = Loc::getMessage('PROJECT_SEO_PROPS_PARTNER_URI');
    }

    public function DoInstall()
    {
        $this->Install();
    }

    public function DoUninstall()
    {
        $this->Uninstall();
    }

    /*
     * InstallEvent
     */

    public function InstallEvent()
    {
        $this->registerEventHandler('iblock', 'OnIBlockPropertyBuildList', 'Project\Seo\Props\Property\SectionFilter',
            'GetIBlockPropertyDescription');
        $this->registerEventHandler('main', 'OnUserTypeBuildList', 'Project\Seo\Props\Property\SectionFilter',
            'GetUserTypeDescription');
    }

    public function UnInstallEvent()
    {
        $this->unRegisterEventHandler('iblock', 'OnIBlockPropertyBuildList', 'Project\Seo\Props\Property\SectionFilter',
            'GetIBlockPropertyDescription');
        $this->unRegisterEventHandler('main', 'OnUserTypeBuildList', 'Project\Seo\Props\Property\SectionFilter',
            'GetUserTypeDescription');
    }

}